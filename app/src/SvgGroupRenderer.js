import React from 'react';

export const SvgGroupRenderer = ({ model, onClick }) => {
    return (
        <g
            onClick={() => onClick(model)}
            className={model.answered ? 'bounceIn' : 'group-hidden'}
            dangerouslySetInnerHTML={{ __html: model.answered ? model.html : '' }}
        />
    );
};
