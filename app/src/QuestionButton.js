import React from 'react';
import svgPath from './assets/exam.svg';
export const QuestionButton = ({ onClick }) => {
    return (
        <img className={'map-button'} src={svgPath} onClick={onClick} />
    );
};
