import React, { useState } from 'react';
import { Modal } from 'antd';
import { QuestRules } from './QuestRules';

export const StartScreen = ({ step, goNext }) => {
    const [visible, changeVisible] = useState(false);
    return (
        <div>
            <div>
                <h2 className={'h4-v1'} onClick={() => changeVisible(!visible)}>
                    Memories
                </h2>

                <Modal centered visible={visible} onOk={() => goNext()} title={'Этапы (и правила) выполнения квеста'} onCancel={() => changeVisible(false)} okText={'Начали!'} cancelText={'Отмена'}>
                    <QuestRules />
                </Modal>
            </div>
        </div>
    );
};
