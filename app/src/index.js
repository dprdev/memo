import 'antd/dist/antd.css';
import React, { useState, useEffect } from 'react';
import ReactDOM from 'react-dom';
import './style.css';
import { StartScreen } from './StartScreen';
import { MapScreen } from './MapScreen';
import { Modal } from 'antd';

const initStorage = () => {
    localStorage.setItem('step', localStorage.getItem('step') || '0');
    localStorage.setItem('answer', localStorage.getItem('answer') || '{}');
};
const useStepper = () => {
    const [step, changeStep] = useState(
        Number(localStorage.getItem('step'))
    );
    localStorage.setItem('step', step);

    return {
        step,
        goNext: () => changeStep(step + 1)
    };
};

function EndGameScreen() {
    const [isVisible, change] = useState(true);
    return (
        <Modal visible={isVisible} onOk={() => change(false)} centered title={'ПОБЕДААА!!!! :)'}>
            Поздравляю с успешным завершением квеста, полосатик. Ты доказала, что помнишь действительно всё, что с нами происходило,
            и поэтому ты можешь забрать свой подарок. Он ждёт тебя у меня дома :)
        </Modal>
    )
};

const App = () => {
    useEffect(initStorage, [])
    const { goNext, step } = useStepper();

    return (
        <div className={'vw flex-center bg'}>
            {step === 0 && <StartScreen goNext={goNext} />}
            {step === 1 && <MapScreen goNext={goNext} />}
            {step === 2 && <EndGameScreen />}
        </div>
    );
};

ReactDOM.render(<App />, document.getElementById('root')); 
