import { Icon, Modal, Collapse, Input } from 'antd';
import React, { useState } from 'react';
import QrReader from 'react-qr-reader';
import { QuestionButton } from './QuestionButton';
import { SvgGroupRenderer } from './SvgGroupRenderer';

const useData = () => {
    const { index = 0 } = JSON.parse(localStorage.getItem('answer'));
    const [state, change] = useState({
        index: index,
        items: [
            require('./data/admin').default,
            require('./data/park_mak').default,
            require('./data/river').default,
            require('./data/arena').default,
            require('./data/park_don').default,
        ]
    });

    return [state, {
        current: state.items[state.index],
        next: () => change({ ...state, index: state.index + 1 }),
        prev: () => change({ ...state, index: state.index - 1 }),
        backupAnswer: (i) => {
            if (state.index === (i - 1)) {
                change({ ...state, index: state.index + 1 });
                localStorage.setItem('answer', JSON.stringify({ index: state.index + 1 }));
            }
        },
        answer: ({ id, text }) => {
            const items = state.items.map((item) => {
                if (item.id === id
                    && item.a === text) {
                    return { ...item, answered: true };
                }

                return item;
            });

            if (items.find((item) => item.id === id).a) {
                localStorage.setItem('answer', JSON.stringify({ index: state.index + 1 }));
                change({
                    index: state.index + 1,
                    items: items
                })
            }
        }
    }]
};

const useModal = () => {
    const [state, change] = useState(false);

    return {
        state,
        show: () => change(true),
        hide: () => change(false)
    }
};

function SvgBackground() {
    return (
        <defs>
            <clipPath id="clip-path" transform="translate(-97.5 -160.45)">
                <path className="cls-1" d="M604.61,751.16q-.25.8-.54,1.59h0q-36.33,31.17-73.38,61.5c-38.47,31.42-25.87,29.25-61.3.29-60.13-49.15-118.7-100.1-176.5-152l.59.14c24.32,5.74,49.56,5.87,74.55,6,15.92.06,32.08.1,47.42-4.16,15.8-4.39,30-13.15,43.25-22.85a310.86,310.86,0,0,0,58.79-56.42c8.47-10.59,16.4-21.9,27.24-30,12.76-9.61,28.81-14.27,42.67-22,4.39,61,12,121.83,18.43,182.67C607.07,727.68,608.24,739.87,604.61,751.16Z" />
            </clipPath>
            <clipPath id="clip-path-2" transform="translate(-97.5 -160.45)">
                <rect className="cls-1" x="618" y="415" width="200" height="200" />
            </clipPath>
            <clipPath id="clip-path-5" transform="translate(-97.5 -160.45)">
                <path id="граница" className="cls-1" d="M899.07,412.38c-5.13,54.85-31.25,103.61-76.82,145.19-72.08,65.79-144.11,131.65-218.18,195.18q.28-.79.54-1.59c3.63-11.29,2.46-23.48,1.21-35.26-6.44-60.84-14-121.68-18.43-182.67-2.11-29.25-3.48-58.53-3.63-87.87-.06-11.6.18-23.68,5.37-34.05,4.84-9.68,13.48-16.83,22.07-23.42q9.34-7.18,19-14h0c23.08,3.25,47.7-2,69,7.32,12.8,5.61,23.27,16.13,36.68,20.08,14.87,4.37,31.45.08,46,5.54,18.06,6.81,27.39,26.25,40.77,40.17,6.32,6.56,14.82,12.34,23.85,11.11,13.17-1.8,19.58-16.45,26-28.11C877.9,420.08,889.17,410.24,899.07,412.38Z" />
            </clipPath>
            <clipPath id="clip-path-6" transform="translate(-97.5 -160.45)">
                <path className="cls-1" d="M630.21,373.92q-9.66,6.78-19,14c-8.59,6.59-17.23,13.74-22.07,23.42-5.19,10.37-5.43,22.45-5.37,34.05.15,29.34,1.52,58.62,3.63,87.87-13.86,7.72-29.91,12.38-42.67,22-10.84,8.15-18.77,19.46-27.24,30a310.86,310.86,0,0,1-58.79,56.42c-13.22,9.7-27.45,18.46-43.25,22.85-15.34,4.26-31.5,4.22-47.42,4.16-25-.1-50.23-.23-74.55-6l-.59-.14h0q-49.29-44.18-97.94-89.08c.75-21.3,4.33-42.1,14.13-60.88,4.14-7.94,9.31-15.32,13.2-23.38,5.08-10.53,7.9-22.07,13.42-32.37,13.72-25.59,42.21-40,70.5-46.49s57.69-6.78,86.14-12.55c4.14-.85,8.36-1.84,11.9-4.16,9.59-6.26,11-19.38,15.48-29.94,8.74-20.73,30.83-32.86,52.73-38s44.76-4.92,66.82-9.37c3.09-.63,6.17-1.34,9.22-2.15.41.54.82,1.11,1.24,1.7,18.89,26.86,44.61,51.54,76.9,57.45Q628.4,373.68,630.21,373.92Z" />
            </clipPath>
            <clipPath id="clip-path-7" transform="translate(-97.5 -160.45)">
                <rect className="cls-1" x="288" y="394" width="240" height="240" />
            </clipPath>
            <clipPath id="clip-path-11" transform="translate(-97.5 -160.45)">
                <path id="граница-2" data-name="граница" className="cls-1" d="M899.07,412.38c-9.9-2.14-21.17,7.7-26.61,17.65-6.38,11.66-12.79,26.31-26,28.11-9,1.23-17.53-4.55-23.85-11.11-13.38-13.92-22.71-33.36-40.77-40.17-14.51-5.46-31.09-1.17-46-5.54-13.41-3.95-23.88-14.47-36.68-20.08-21.32-9.35-45.94-4.07-69-7.32h0q-1.82-.24-3.6-.57c-32.29-5.91-58-30.59-76.9-57.45-.42-.59-.83-1.16-1.24-1.7-18-24.22-26.42-4-47-28.7a8.26,8.26,0,0,1-1.79-5.52c-.06-6.76.16-13.52.08-20.27,16.64-19.07,32.75-37.55,52.07-52.86,120.79-95.63,304-26.7,340.82,126.13Q902.67,374.52,899.07,412.38Z" />
            </clipPath>
            <clipPath id="clip-path-12" transform="translate(-97.5 -160.45)">
                <rect className="cls-1" x="641" y="192" width="200" height="200" />
            </clipPath>
            <clipPath id="clip-path-15" transform="translate(-97.5 -160.45)">
                <path id="шраница" className="cls-1" d="M548.47,314.2c-3.05.81-6.13,1.52-9.22,2.15-22.06,4.45-44.91,4.21-66.82,9.37s-44,17.28-52.73,38c-4.46,10.56-5.89,23.68-15.48,29.94-3.54,2.32-7.76,3.31-11.9,4.16-28.45,5.77-57.85,6-86.14,12.55s-56.78,20.9-70.5,46.49c-5.52,10.3-8.34,21.84-13.42,32.37-3.89,8.06-9.06,15.44-13.2,23.38-9.8,18.78-13.38,39.58-14.13,60.88q-12.13-11.14-24.24-22.32c-71-65.49-90.23-169.89-49.87-258.14,40-87.38,130.17-139.25,227.09-128.55,54.24,6,97.44,34.2,133.48,74.11,6.06,6.72,11.74,13.78,18.18,21.37l.22-.26c.08,6.75-.14,13.51-.08,20.27a8.26,8.26,0,0,0,1.79,5.52C522.05,310.18,530.44,290,548.47,314.2Z" />
            </clipPath>
            <symbol id="Новый_символ" data-name="Новый символ" viewBox="0 0 18.96 34.16">
                <line className="cls-2" x1="0.35" y1="10.15" x2="10.15" y2="0.35" />
                <line className="cls-2" x1="18.6" y1="9.9" x2="9.6" y2="0.9" />
                <line className="cls-2" x1="0.35" y1="19.15" x2="10.15" y2="9.35" />
                <line className="cls-2" x1="18.6" y1="18.9" x2="9.6" y2="9.9" />
                <line className="cls-2" x1="0.35" y1="28.15" x2="10.15" y2="18.35" />
                <line className="cls-2" x1="18.6" y1="27.9" x2="9.6" y2="18.9" />
                <line className="cls-2" x1="9.6" y1="0.15" x2="9.6" y2="34.15" />
            </symbol>
            <symbol id="Новый_символ_2" data-name="Новый символ 2" viewBox="0 0 25.51 40">
                <path d="M0,22.59c0-5.36,0-10.73,0-16.1A1.37,1.37,0,0,1,1.08,4.92Q7.85,2.61,14.57.16c1.07-.39,1.19-.06,1.18.9,0,4,0,8,0,12a2.38,2.38,0,0,0,1.44,2.55c2.51,1.28,4.91,2.77,7.39,4.11a1.59,1.59,0,0,1,1,1.6c0,5.77,0,11.55,0,17.33,0,1-.23,1.35-1.29,1.34-7.69,0-15.38,0-23.07,0C.25,40,0,39.73,0,38.84,0,33.42,0,28,0,22.59Zm7.74-5c1.5,0,3,0,4.5,0,.71,0,1.11-.14,1.09-1s-.43-.88-1-.88c-3,0-6,0-9,0-.7,0-1.11.15-1.09,1s.42.89,1,.88C4.76,17.6,6.26,17.61,7.76,17.61Zm.13,19.32c1.45,0,2.91,0,4.36,0,.7,0,1.11-.13,1.1-1s-.43-.88-1-.87c-3,0-6,0-9,0-.71,0-1.11.15-1.11,1s.44.88,1,.87ZM7.75,9.86c1.41,0,2.82,0,4.23,0,.65,0,1.4.19,1.38-.94S12.65,8,12,8H3.59c-.67,0-1.4-.18-1.38.94s.7.87,1.32.87Zm0,3.89c1.5,0,3,0,4.5,0,.7,0,1.12-.15,1.1-1s-.41-.89-1-.89q-4.5,0-9,0c-.7,0-1.11.15-1.1,1s.54.9,1.18.89Zm0,7.72H12.1c.66,0,1.27.07,1.25-.93s-.59-.86-1.19-.86c-3,0-5.91,0-8.86,0-.67,0-1.12.06-1.1.93s.57.87,1.18.87Zm0,2.09H3.4c-.58,0-1.19-.13-1.19.83s.45.93,1.12.92h9c.6,0,1,0,1.06-.83s-.46-.94-1.12-.93C10.74,23.57,9.25,23.56,7.75,23.56ZM7.81,33c1.5,0,3,0,4.51,0,.63,0,1-.1,1-.88s-.36-.91-1-.9q-4.58,0-9.16,0c-.71,0-1,.26-1,1s.35.84.94.83Zm-.2-3.87c1.59,0,3.17,0,4.76,0,.67,0,1-.19,1-.92s-.31-.84-.92-.83c-3.08,0-6.16,0-9.24,0-.68,0-1,.18-1,.91s.46.84,1,.84C4.71,29.14,6.16,29.15,7.61,29.15Zm14.48,8.14c1,.29,1.15-.22,1.17-1.14s-.21-1.38-1.28-1.34-1.27.27-1.27,1.25S21,37.54,22.09,37.29ZM18.87,23.86c.3-1-.26-1.14-1.16-1.15s-1.3.18-1.29,1.19.31,1.35,1.32,1.33S19.07,24.87,18.87,23.86Zm4.34.11c.2-.93-.14-1.28-1.12-1.26s-1.41.11-1.39,1.23.38,1.3,1.35,1.29S23.42,24.94,23.21,24Zm-5.64,9.27c.84.12,1.42,0,1.36-1.07,0-.86-.05-1.47-1.21-1.42-.9.05-1.34.23-1.3,1.23C16.46,32.8,16.48,33.48,17.57,33.24Zm5.64-1.07c.15-.92,0-1.48-1.16-1.41-.83,0-1.36.11-1.34,1.17s.14,1.44,1.25,1.36C22.74,33.22,23.47,33.29,23.21,32.17Zm-4.33,4c.19-1-.13-1.39-1.2-1.36-.92,0-1.27.28-1.27,1.23s.23,1.38,1.29,1.32C18.58,37.32,19.12,37.19,18.88,36.17ZM23.2,28c.16-.81,0-1.36-1.05-1.29-.81.05-1.47,0-1.44,1.17,0,1,.25,1.37,1.29,1.32C22.82,29.19,23.46,29.15,23.2,28Zm-4.32,0c.2-.95-.17-1.29-1.14-1.27s-1.33.15-1.33,1.17.27,1.35,1.31,1.32S19.1,29,18.88,28Z" />
            </symbol>
            <symbol id="Новый_символ_1" data-name="Новый символ 1" viewBox="0 0 23.26 30">
                <path d="M11.57,30c-3.34,0-6.68,0-10,0-.57,0-1,0-1-.79s.29-.92.92-.83c1,.15,1.23-.36,1.22-1.28,0-5.9,0-11.79,0-17.68A2.12,2.12,0,0,1,4,7.23C6.29,6,8.55,4.64,10.81,3.3a1.46,1.46,0,0,1,1.71,0q3.63,2.19,7.34,4.24A1.46,1.46,0,0,1,20.7,9c0,6,0,12,0,18,0,.92.08,1.55,1.23,1.37.72-.11.68.41.7.9s-.25.72-.78.72C18.4,30,15,30,11.57,30Zm-5-6.69c0,2.24,0,2.24,2.24,2.24s2.19,0,2.17-2.2c0-2.64.3-2.21-2.25-2.23C6.53,21.08,6.53,21.1,6.53,23.3Zm8-7.92c-2.27,0-2.27,0-2.27,2.21s0,2.2,2.17,2.2,2.27,0,2.27-2.22S16.69,15.38,14.52,15.38Zm0,10.16c2.2,0,2.2,0,2.2-2.14v-.12c0-2.46.37-2.17-2.17-2.17s-2.26-.34-2.26,2.2S12,25.49,14.49,25.54Zm-8-8c0,2.25,0,2.27,2.21,2.25,2.55,0,2.17.36,2.2-2.15,0-2.25,0-2.25-2.22-2.25S6.53,15.38,6.53,17.53Zm0-5.77C6.53,14,6.53,14,8.81,14h.11c2,0,2,0,2-2.08,0-2.26,0-2.26-2.29-2.26C6.53,9.69,6.53,9.69,6.53,11.76Zm7.89-2.07c-2.17,0-2.17,0-2.17,1.93,0,2.41,0,2.41,2.39,2.41,2.05,0,2.05,0,2.05-2C16.69,9.69,16.69,9.69,14.42,9.69Z" />
                <path d="M23.26,6.75C23,7.4,22.82,7.9,22,7.4c-3.13-1.87-6.32-3.65-9.46-5.5a1.52,1.52,0,0,0-1.8,0C7.62,3.73,4.48,5.51,1.35,7.31,1,7.54.57,8,.14,7.21s.17-.87.57-1.1C4.07,4.16,7.45,2.23,10.8.27a1.33,1.33,0,0,1,1.58,0c3.38,2,6.79,3.93,10.18,5.9C22.85,6.31,23.23,6.4,23.26,6.75Z" />
            </symbol>
        </defs>
    )
}

function Heart({ items, index }) {
    function isOpen(itemIndex, item) {
        return itemIndex < index
            ? { ...item, answered: true }
            : item;
    };

    return (
        <svg
            xmlns="http://www.w3.org/2000/svg"
            xmlnsXlink="http://www.w3.org/1999/xlink"
            viewBox="0 0 805 679.11"
            className={'heart'}
        >
            <SvgBackground />
            {items.map((e, i) => (
                <SvgGroupRenderer key={e.id} model={isOpen(i, e)} />
            ))}
        </svg>
    )
}

function SubmitInput({ handle }) {
    const inputRef = React.useRef();
    return (
        <Input type={'number'} ref={inputRef} addonAfter={
            <Icon type="check-square" theme="twoTone" onClick={() => {
                if (inputRef.current
                    && inputRef.current.input
                    && inputRef.current.input.value) {
                    handle(Number(inputRef.current.input.value));
                }
            }} />
        } />
    )
}

export const MapScreen = ({ goNext }) => {
    const [state, { current, answer, ...data }] = useData();
    const { show, state: modalState, hide } = useModal();
    const [isQrModalOpen, changeQrModalState] = useState(false);

    const makeAnswer = (resolver) => {
        Promise.resolve()
            .then(() => hide() || changeQrModalState(false))
            .then(() => new Promise((r) => setTimeout(() => r(), 250)))
            .then(() => resolver());
    };

    return (
        <div className={'vw flex-center'}>
            <Heart items={state.items} index={state.index} />
            {state.index === 5 && setTimeout(goNext, 1200)}
            {current && (
                <React.Fragment>
                    <QuestionButton onClick={show} />
                    <div style={{ position: 'fixed', bottom: '-.25rem', left: '1rem', fontSize: '4rem' }}>
                        <Icon type="camera" theme="twoTone" onClick={() => changeQrModalState(!isQrModalOpen)} />
                    </div>

                    <Modal
                        centered={true}
                        okText={'Закрыть'}
                        onOk={hide}
                        title={`${state.index + 1} вопрос из 5`}
                        style={{ maxWidth: '360px' }}
                        visible={modalState}
                    >
                        <div>
                            {current.q}
                        </div>
                    </Modal>

                    <Modal
                        centered={true}
                        okText={'Закрыть'}
                        onOk={() => changeQrModalState(false)}
                        style={{ maxWidth: '380px' }}
                        visible={isQrModalOpen}
                        destroyOnClose={true}
                    >
                        <Collapse bordered={false} defaultActiveKey={['1']} accordion>
                            <Collapse.Panel header={'QR Code'} key={'1'}>
                                <QrReader
                                    style={{ width: '100%' }}
                                    onError={console.log}
                                    onScan={data => data && makeAnswer(() => answer({ id: current.id, text: data }))}
                                />
                            </Collapse.Panel>
                            <Collapse.Panel header={'Ввести ответ вручную'} key={'2'}>
                                <SubmitInput handle={value => makeAnswer(() => data.backupAnswer(value))} />
                            </Collapse.Panel>
                        </Collapse>
                    </Modal>
                </React.Fragment>
            )}
        </div>
    );
};
